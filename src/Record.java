import java.time.LocalDateTime;

public class Record extends Database {
    private LocalDateTime addTime;
    private LocalDateTime timeValid;
    private Integer id;
    private String caseInformation;

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public LocalDateTime getTimeValid() {
        return timeValid;
    }

    public Record(LocalDateTime addTime, LocalDateTime timeValid, Integer id, String caseInformation) {
        this.addTime = addTime;
        this.timeValid = timeValid;
        this.id = id;
        this.caseInformation = caseInformation;
    }
}
