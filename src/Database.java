import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Database {
    Map <Integer, Record> database = new HashMap();

    public Database(Map database) {
        this.database = database;
    }

    public Database() {
    }

    public void addRecord(Integer id, String caseInformation){
        LocalDateTime addTime = LocalDateTime.now();
        LocalDateTime timeValid = LocalDateTime.now().plusSeconds(30);
        database.put(id, new Record(addTime, timeValid, id, caseInformation));
    }
    public void findRecord (Integer key) {
        System.out.println(database.get(key));
    }
    public void refresh() {
        Iterator<Map.Entry<Integer, Record>> iterator = database.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, Record> databaseRecord= iterator.next();
            Record record = databaseRecord.getValue();
            if (record.getTimeValid().isAfter(record.getAddTime())) {
                database.remove(databaseRecord.getKey());
            }
        }
    }
}
