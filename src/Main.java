import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Database database = new Database();
        String inputLine;
        do {
            inputLine = scanner.nextLine();
            String inputWord[] = inputLine.trim().split(" ");
            switch (inputWord[0]){
                case "add":
                    database.addRecord(Integer.parseInt(inputWord[1]), inputWord[2]);
                    break;
                case "find":
                    database.findRecord(Integer.parseInt(inputWord[1]));
                    break;
                case "refresh":
                    database.refresh();
                    break;
            }
        }
        while (!inputLine.equals("quit"));
    }
}
